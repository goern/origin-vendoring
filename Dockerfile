FROM registry.access.redhat.com/rhel7-atomic

COPY build/origin-vendoring-playground /

USER nobody

EXPOSE 8080

CMD /origin-vendoring-playground