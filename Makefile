SHELL := /bin/bash
NAME := origin-vendoring-playground
GO := GO15VENDOREXPERIMENT=1 go
GO_VERSION := $(shell $(GO) version | sed -e 's/^[^0-9.]*\([0-9.]*\).*/\1/')
ROOT_PACKAGE := $(shell $(GO) list .)
GOFILES=$(shell find . -type f -name '*.go')
VENDOR_DIR=vendor

VERSION := $(shell cat VERSION)
REV := $(shell git rev-parse --short HEAD 2> /dev/null  || echo 'unknown')
BRANCH := $(shell git rev-parse --abbrev-ref HEAD 2> /dev/null  || echo 'unknown')
BUILD_DATE := $(shell date -u +%Y-%m-%dT%H:%M:%SZ)
BUILDFLAGS := -ldflags \
  " -X $(ROOT_PACKAGE)/version.Version=$(VERSION)\
		-X $(ROOT_PACKAGE)/version.Revision='$(REV)'\
		-X $(ROOT_PACKAGE)/version.Branch='$(BRANCH)'\
		-X $(ROOT_PACKAGE)/version.BuildDate='$(BUILD_DATE)'\
		-X $(ROOT_PACKAGE)/version.GoVersion='$(GO_VERSION)'"
CGO_ENABLED = 0


origin-vendoring-playground: $(GOFILES)
	@echo "building $@"
	@rm -rf build
	CGO_ENABLED=$(CGO_ENABLED) $(GO) build $(BUILDFLAGS) -o build/$@ ./cmd/origin-vendoring-playground

vendoring:
	GO15VENDOREXPERIMENT=1 glide update --strip-vendor --strip-vcs

test:
	CGO_ENABLED=$(CGO_ENABLED) $(GO) test gitlab.com/goern/origin-vendoring/cmd

clean:
	rm -rf build 

dist-clean: clean
	rm -rf vendor
	

.PHONY: clean test vendoring
.DEFAULT: origin-vendoring-playground