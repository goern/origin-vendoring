# Origin Vendoring Playground

You will need glide to do the heavy lifting of vendoring, install it: `go get github.com/Masterminds/glide`

If you want to deploy the current release to OpenShift, try a packaged one... `oc new-app registry.gitlab.com/goern/origin-vendoring/playground:latest`