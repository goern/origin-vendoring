/*
   origin-vendoring-playground
   Copyright (C) 2017 Christoph Görn

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"k8s.io/kubernetes/pkg/client/restclient"

	"gitlab.com/goern/origin-vendoring/version"

	log "github.com/Sirupsen/logrus"
	"github.com/blang/semver"
	"github.com/openshift/origin/pkg/cmd/util/clientcmd"
	"github.com/spf13/pflag"
)

var (
	appVersion semver.Version
)

func requestHandler(w http.ResponseWriter, r *http.Request) {
	log.Info("received a HTTP request")

	w.Header().Set("Content-Type", "application/json")

	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	w.Write([]byte("{\"status\": \"OK\", \"version\": \"" + appVersion.String() + "\"}"))
	w.WriteHeader(http.StatusOK)
}

func main() {
	log.SetLevel(log.DebugLevel)

	appVersion, _ = version.GetSemverVersion()
	log.Infof("This is origin-vendoring-playground %s - a tiny demo how to talk to OpenShift...", appVersion.String())

	// lets get the Kubernetes configuration available inside the cluster...
	kconfig, err := restclient.InClusterConfig()
	if err != nil {
		log.Fatal(err)
		return
	}
	log.Debug(kconfig)

	oclient := clientcmd.New(pflag.NewFlagSet("empty", pflag.ContinueOnError))
	restclient, err := oclient.RESTClient()
	if err != nil {
		log.Fatal(err)
		return
	}
	log.Info(restclient.APIVersion().String())

	// Mechanical domain.
	errc := make(chan error)
	//ctx := context.Background()

	// Interrupt handler.
	go func() {
		log.Info("setting interrupt handlers")

		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
		errc <- fmt.Errorf("%s", <-c)
	}()

	// HTTP transport.
	go func() {
		log.Info("starting HTTP transport")

		http.HandleFunc("/", requestHandler)
		errc <- http.ListenAndServe(":8080", nil)
	}()

	log.Error("exit", <-errc)
}
